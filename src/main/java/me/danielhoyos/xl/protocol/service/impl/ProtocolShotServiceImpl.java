package me.danielhoyos.xl.protocol.service.impl;

import java.util.HashMap;
import java.util.Iterator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import me.danielhoyos.xl.commons.exception.XLException;
import me.danielhoyos.xl.commons.model.Game;
import me.danielhoyos.xl.commons.model.SelfPlayer;
import me.danielhoyos.xl.commons.rest.model.GameResponse;
import me.danielhoyos.xl.commons.rest.model.ShotRequest;
import me.danielhoyos.xl.commons.rest.model.ShotResponse;
import me.danielhoyos.xl.commons.service.GameService;
import me.danielhoyos.xl.commons.service.impl.AutopilotRunnableImpl;
import me.danielhoyos.xl.commons.utils.RulesUtils;
import me.danielhoyos.xl.commons.utils.ShipUtils;
import me.danielhoyos.xl.commons.utils.ShotUtils;
import me.danielhoyos.xl.protocol.service.ProtocolShotService;
import me.danielhoyos.xl.user.wsclient.ProtocolClient;

/**
 * @author Daniel Hoyos
 *
 */
@Service
public class ProtocolShotServiceImpl implements ProtocolShotService {

	@Autowired
	GameService gameService;

	@Autowired
	ProtocolClient client;

	@Autowired
	AutopilotRunnableImpl autopilot;

	@Override
	public ShotResponse receiveShot(String gameId, ShotRequest salvo) throws XLException {
		ShotResponse response = new ShotResponse();
		boolean kill = false;

		if (gameId.isEmpty()) {
			throw new XLException("Missing 'game_id' parameter", HttpStatus.BAD_REQUEST);
		}

		Game game = gameService.retreiveGame(gameId);
		ShotUtils.validateTurn(game);

		SelfPlayer self = game.getSelf();
		HashMap<String, String> salvoResponse = new HashMap<>();

		for (Iterator<String> i = salvo.getSalvo().iterator(); i.hasNext();) {
			String shot = i.next();
			String result = ShotUtils.processShot(shot, self);
			if (!salvoResponse.containsKey(shot)) {
				salvoResponse.put(shot, result);
				kill = true;
			}
		}

		game.setSelf(self);
		game.setRules(RulesUtils.updateRulesShots(game));

		GameResponse gameResponse = new GameResponse();

		if (ShipUtils.allShipsDestroyed(self.getShips())) {
			game.setActive(false);
			game.setWon(game.getOpponent().getUserId());
			gameResponse.setWon(game.getOpponent().getUserId());
		} else {
			String turn;
			if (RulesUtils.SUPERCHARGE.equals(game.getRules().getRulesName()) && kill) {
				turn = game.getOpponent().getUserId();
			} else {
				turn = game.getSelf().getUserId();
			}
			game.setTurn(turn);
			gameResponse.setPlayerTurn(turn);
		}

		// BoardUtils.buildBoard(game); // TODO Delete

		gameService.saveGame(game);

		response.setSalvo(salvoResponse);
		response.setGame(gameResponse);

		if (game.isAutopilotActive()) {
			Thread newThread = new Thread(autopilot, game.getGameId());
			newThread.start();
		}

		return response;
	}

}
