package me.danielhoyos.xl.protocol.service;

import me.danielhoyos.xl.commons.exception.XLException;
import me.danielhoyos.xl.commons.rest.model.ShotRequest;
import me.danielhoyos.xl.commons.rest.model.ShotResponse;

/**
 * @author Daniel Hoyos
 *
 */
public interface ProtocolShotService {

	ShotResponse receiveShot(String gameId, ShotRequest salvo) throws XLException;

}
