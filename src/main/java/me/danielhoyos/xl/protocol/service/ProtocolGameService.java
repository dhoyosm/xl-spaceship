package me.danielhoyos.xl.protocol.service;

import me.danielhoyos.xl.commons.exception.XLException;
import me.danielhoyos.xl.commons.rest.model.NewGameRequest;
import me.danielhoyos.xl.commons.rest.model.NewGameResponse;

/**
 * @author Daniel Hoyos
 *
 */
public interface ProtocolGameService {

	NewGameResponse createGame(NewGameRequest input) throws XLException;

}
