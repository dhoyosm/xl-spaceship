package me.danielhoyos.xl.protocol.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import me.danielhoyos.xl.commons.exception.XLException;
import me.danielhoyos.xl.commons.model.Game;
import me.danielhoyos.xl.commons.model.OpponentPlayer;
import me.danielhoyos.xl.commons.model.Player;
import me.danielhoyos.xl.commons.model.SelfPlayer;
import me.danielhoyos.xl.commons.repository.PlayerRepository;
import me.danielhoyos.xl.commons.rest.model.NewGameRequest;
import me.danielhoyos.xl.commons.rest.model.NewGameResponse;
import me.danielhoyos.xl.commons.service.GameService;
import me.danielhoyos.xl.commons.utils.GameUtils;
import me.danielhoyos.xl.commons.utils.MathUtils;
import me.danielhoyos.xl.commons.utils.RulesUtils;
import me.danielhoyos.xl.protocol.service.ProtocolGameService;

/**
 * @author Daniel Hoyos
 *
 */
@Service
public class ProtocolGameServiceImpl implements ProtocolGameService {

	private static final String GAME_PREFIX = "match-";

	@Autowired
	GameService gameService;

	@Autowired
	PlayerRepository playerRepository;

	@Override
	public NewGameResponse createGame(NewGameRequest input) throws XLException {

		Player player = playerRepository.findAll().get(0);

		SelfPlayer self = GameUtils.createSelfPlayer(player);
		OpponentPlayer opponent = GameUtils.createOpponentPlayer(input.getUserId(), input.getFullName());
		String[] players = { self.getUserId(), opponent.getUserId() };
		String turn = players[MathUtils.randomGenerator(2)];

		Game game = new Game();
		game.setGameId(GAME_PREFIX + System.currentTimeMillis());
		game.setOpponent(opponent);
		game.setSelf(self);
		game.setSpaceshipProtol(input.getSpaceshipProtocol());
		game.setActive(true);
		game.setTurn(turn);
		game.setRules(RulesUtils.newRules(input.getRules()));

		NewGameResponse response = new NewGameResponse();
		response.setUserId(self.getUserId());
		response.setFullName(self.getFullName());
		response.setGameId(game.getGameId());
		response.setStarting(turn);
		response.setRules(game.getRules().getRulesName());

		gameService.saveGame(game);

		return response;
	}

}
