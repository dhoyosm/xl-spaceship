package me.danielhoyos.xl.protocol.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import me.danielhoyos.xl.commons.exception.XLException;
import me.danielhoyos.xl.commons.rest.model.NewGameRequest;
import me.danielhoyos.xl.commons.rest.model.NewGameResponse;
import me.danielhoyos.xl.commons.rest.model.ShotRequest;
import me.danielhoyos.xl.commons.rest.model.ShotResponse;
import me.danielhoyos.xl.protocol.service.ProtocolGameService;
import me.danielhoyos.xl.protocol.service.ProtocolShotService;

/**
 * @author Daniel Hoyos
 *
 */
@RestController
@RequestMapping("/xl-spaceship/protocol")
public class ProtocolController {

	@Autowired
	private ProtocolGameService gameService;

	@Autowired
	private ProtocolShotService shotService;

	@ExceptionHandler(MethodArgumentNotValidException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@RequestMapping(value = "/game/new", method = RequestMethod.POST)
	public ResponseEntity<NewGameResponse> createGame(@Valid @RequestBody NewGameRequest input) throws XLException {

		NewGameResponse body = gameService.createGame(input);

		return new ResponseEntity<NewGameResponse>(body, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/game/{game_id}", method = RequestMethod.POST)
	public ResponseEntity<ShotResponse> receiveShot(@PathVariable("game_id") String gameId,
			@RequestBody ShotRequest salvo) throws XLException {

		ShotResponse body = shotService.receiveShot(gameId, salvo);

		return new ResponseEntity<ShotResponse>(body, HttpStatus.OK);
	}

}
