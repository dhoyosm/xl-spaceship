package me.danielhoyos.xl.commons.utils;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Pattern;

import me.danielhoyos.xl.commons.exception.XLException;
import me.danielhoyos.xl.commons.model.Coordinate;
import me.danielhoyos.xl.commons.model.Game;
import me.danielhoyos.xl.commons.model.OpponentPlayer;
import me.danielhoyos.xl.commons.model.SelfPlayer;
import me.danielhoyos.xl.commons.model.Ship;

public class ShotUtils {

	private static final String SALVO_PATTERN = "[0-9a-fA-F][xX][0-9a-fA-F]";

	private static final String MISSED_SHOT = "miss";
	private static final String HIT_SHOT = "hit";
	private static final String KILL_SHOT = "kill";

	public static void validateTurn(Game game) throws XLException {
		if (!game.isActive()) {
			throw new XLException("Game '" + game.getGameId() + "' is over");
		}

		if (!game.getTurn().equals(game.getOpponent().getUserId())) {
			throw new XLException("It is your opponent's turn. Please wait for your turn");
		}
	}

	public static void validateShotCoordinate(String shot) throws XLException {
		Pattern pattern = Pattern.compile(SALVO_PATTERN);
		if (!pattern.matcher(shot).matches()) {
			throw new XLException("Malformed shot: " + shot);
		}
	}

	public static String processShot(String shot, SelfPlayer self) throws XLException {
		validateShotCoordinate(shot);

		int row = Integer.parseInt(shot.substring(0, 1), 16);
		int column = Integer.parseInt(shot.substring(2, 3), 16);

		Coordinate shootCoordinate = new Coordinate(row, column);

		List<Coordinate> missedShots = self.getMissedShots();

		if (missedShots.contains(shootCoordinate)) {
			return MISSED_SHOT;
		}

		List<Ship> ships = self.getShips();
		for (int i = 0; i < ships.size(); i++) {
			Ship ship = ships.get(i);
			List<Coordinate> hits = ship.getHits();
			if (hits.contains(shootCoordinate)) {
				return MISSED_SHOT;
			}
			if (ship.getPosition().contains(shootCoordinate)) {
				hits.add(shootCoordinate);
				ship.setHits(hits);
				ships.set(i, ship);
				self.setShips(ships);
				if (ship.isDestroyed()) {
					return KILL_SHOT;
				}
				return HIT_SHOT;
			}
		}
		missedShots.add(shootCoordinate);
		self.setMissedShots(missedShots);
		return MISSED_SHOT;
	}

	public static OpponentPlayer processShotResponse(HashMap<String, String> salvo, OpponentPlayer opponent)
			throws XLException {
		List<Coordinate> hitShots = opponent.getHitShots();
		List<Coordinate> missedShots = opponent.getMissedShots();
		for (Iterator<String> i = salvo.keySet().iterator(); i.hasNext();) {
			String shot = i.next();
			validateShotCoordinate(shot);
			int row = Integer.parseInt(shot.substring(0, 1), 16);
			int column = Integer.parseInt(shot.substring(2, 3), 16);
			Coordinate shootCoordinate = new Coordinate(row, column);
			if (MISSED_SHOT.equals(salvo.get(shot))) {
				missedShots.add(shootCoordinate);
			} else if (HIT_SHOT.equals(salvo.get(shot))) {
				hitShots.add(shootCoordinate);
			} else if (KILL_SHOT.equals(salvo.get(shot))) {
				hitShots.add(shootCoordinate);
			}
		}

		opponent.setHitShots(hitShots);
		opponent.setMissedShots(missedShots);

		return opponent;
	}
}
