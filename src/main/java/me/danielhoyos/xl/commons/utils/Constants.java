/**
 * 
 */
package me.danielhoyos.xl.commons.utils;

/**
 * @author Daniel Hoyos
 *
 */
public class Constants {
	public static final int ROW_SIZE = 16;
	public static final int COLUMN_SIZE = 16;
}
