package me.danielhoyos.xl.commons.utils;

import java.util.regex.Pattern;

import me.danielhoyos.xl.commons.exception.XLException;
import me.danielhoyos.xl.commons.model.Game;
import me.danielhoyos.xl.commons.model.Rules;

/**
 * @author Daniel Hoyos
 *
 */
public class RulesUtils {

	public final static Rules DEFAULT_RULES = new Rules("standard", 5);
	public final static String STANDARD = "standard";
	public final static String SUPERCHARGE = "super-charge";
	public final static String DESPERATION = "desperation";
	private final static String X_SHOT = "([1-9]|10)-shot";

	public static Rules newRules(String rulesName) throws XLException {
		Pattern pattern = Pattern.compile(X_SHOT);

		if (rulesName == null || rulesName.isEmpty()) {
			return DEFAULT_RULES;
		}
		if (STANDARD.equals(rulesName) || SUPERCHARGE.equals(rulesName)) {
			return new Rules(rulesName, 5);
		} else if (DESPERATION.equals(rulesName)) {
			return new Rules(rulesName, 1);
		} else if (pattern.matcher(rulesName).matches()) {
			int shots = new Integer(rulesName.split("-")[0]);
			return new Rules(rulesName, shots);
		} else {
			throw new XLException("'" + rulesName + "' is not a valid rule");
		}
	}

	public static Rules updateRulesShots(Game game) {
		Rules rules = game.getRules();
		if (STANDARD.equals(rules.getRulesName())) {
			rules.setShots(ShipUtils.getShipsAliveCount(game.getSelf().getShips()));
		} else if (DESPERATION.equals(rules.getRulesName())) {
			rules.setShots(rules.getShots() + ShipUtils.getShipsAliveCount(game.getSelf().getShips()));
		}
		return rules;
	}

}
