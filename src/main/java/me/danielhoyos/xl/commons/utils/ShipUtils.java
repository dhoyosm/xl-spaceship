package me.danielhoyos.xl.commons.utils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import me.danielhoyos.xl.commons.model.Coordinate;
import me.danielhoyos.xl.commons.model.Ship;

/**
 * @author Daniel Hoyos
 *
 */
public class ShipUtils {

	public enum Type {
		A_CLASS, B_CLASS, S_CLASS, ANGLE, WINGER
	}

	public enum Direction {
		DEG_0, DEG_90, DEG_180, DEG_270
	}

	public static List<Coordinate> getAClassInitialPosition(Direction dir, int iniRow, int iniColumn) {
		switch (dir) {
		case DEG_0:
			int rows0[][] = { { 1 }, { 0, 2 }, { 0, 1, 2 }, { 0, 2 } };
			return getPosition(rows0, iniRow, iniColumn);
		case DEG_90:
			int rows90[][] = { { 0, 1, 2 }, { 1, 3 }, { 0, 1, 2 } };
			return getPosition(rows90, iniRow, iniColumn);
		case DEG_180:
			int rows180[][] = { { 0, 2 }, { 0, 1, 2 }, { 0, 2 }, { 1 } };
			return getPosition(rows180, iniRow, iniColumn);
		case DEG_270:
			int rows270[][] = { { 1, 2, 3 }, { 0, 2 }, { 1, 2, 3 } };
			return getPosition(rows270, iniRow, iniColumn);
		default:
			return new ArrayList<>();
		}
	}

	public static List<Coordinate> getAClassHoleInitialPosition(Direction dir, int iniRow, int iniColumn) {
		switch (dir) {
		case DEG_0:
			int rows0[][] = { {}, { 1 } };
			return getPosition(rows0, iniRow, iniColumn);
		case DEG_90:
			int rows90[][] = { {}, { 2 } };
			return getPosition(rows90, iniRow, iniColumn);
		case DEG_180:
			int rows180[][] = { {}, {}, { 1 } };
			return getPosition(rows180, iniRow, iniColumn);
		case DEG_270:
			int rows270[][] = { {}, { 1 } };
			return getPosition(rows270, iniRow, iniColumn);
		default:
			return new ArrayList<>();
		}
	}

	public static List<Coordinate> getBClassInitialPosition(Direction dir, int iniRow, int iniColumn) {
		switch (dir) {
		case DEG_0:
			int rows0[][] = { { 0, 1 }, { 0, 2 }, { 0, 1 }, { 0, 2 }, { 0, 1 } };
			return getPosition(rows0, iniRow, iniColumn);
		case DEG_90:
			int rows90[][] = { { 0, 1, 2, 3, 4 }, { 0, 2, 4 }, { 1, 3 } };
			return getPosition(rows90, iniRow, iniColumn);
		case DEG_180:
			int rows180[][] = { { 1, 2 }, { 0, 2 }, { 1, 2 }, { 0, 2 }, { 1, 2 } };
			return getPosition(rows180, iniRow, iniColumn);
		case DEG_270:
			int rows270[][] = { { 1, 3 }, { 0, 2, 4 }, { 0, 1, 2, 3, 4 } };
			return getPosition(rows270, iniRow, iniColumn);
		default:
			return new ArrayList<>();
		}
	}

	public static List<Coordinate> getSClassInitialPosition(Direction dir, int iniRow, int iniColumn) {
		switch (dir) {
		case DEG_0:
		case DEG_180:
			int rows0[][] = { { 1, 2 }, { 0 }, { 1, 2 }, { 3 }, { 1, 2 } };
			return getPosition(rows0, iniRow, iniColumn);
		case DEG_90:
		case DEG_270:
			int rows90[][] = { { 3 }, { 0, 2, 4 }, { 0, 2, 4 }, { 1 } };
			return getPosition(rows90, iniRow, iniColumn);
		default:
			return new ArrayList<>();
		}
	}

	public static List<Coordinate> getAngleInitialPosition(Direction dir, int iniRow, int iniColumn) {
		switch (dir) {
		case DEG_0:
			int rows0[][] = { { 0 }, { 0 }, { 0 }, { 0, 1, 2 } };
			return getPosition(rows0, iniRow, iniColumn);
		case DEG_90:
			int rows90[][] = { { 0, 1, 2, 3 }, { 0 }, { 0 } };
			return getPosition(rows90, iniRow, iniColumn);
		case DEG_180:
			int rows180[][] = { { 0, 1, 2 }, { 2 }, { 2 }, { 2 } };
			return getPosition(rows180, iniRow, iniColumn);
		case DEG_270:
			int rows270[][] = { { 3 }, { 3 }, { 0, 1, 2, 3 } };
			return getPosition(rows270, iniRow, iniColumn);
		default:
			return new ArrayList<>();
		}
	}

	public static List<Coordinate> getWingerInitialPosition(Direction dir, int iniRow, int iniColumn) {
		switch (dir) {
		case DEG_0:
		case DEG_180:
			int rows0[][] = { { 0, 2 }, { 0, 2 }, { 1 }, { 0, 2 }, { 0, 2 } };
			return getPosition(rows0, iniRow, iniColumn);
		case DEG_90:
		case DEG_270:
			int rows90[][] = { { 0, 1, 3, 4 }, { 2 }, { 0, 1, 3, 4 } };
			return getPosition(rows90, iniRow, iniColumn);
		default:
			return new ArrayList<>();
		}
	}

	private static List<Coordinate> getPosition(int rows[][], int iniRow, int iniColumn) {
		List<Coordinate> position = new ArrayList<>();
		if (iniRow + rows.length >= Constants.ROW_SIZE) {
			return position;
		}
		for (int i = 0; i < rows.length; i++) {

			for (int j = 0; j < rows[i].length; j++) {
				int column = rows[i][j] + iniColumn;
				if (column >= Constants.COLUMN_SIZE) {
					return new ArrayList<>();
				}
				position.add(new Coordinate(i + iniRow, column));
			}
		}
		return position;
	}

	public static List<Coordinate> getAllShipPositions(List<Ship> ships) {
		List<Coordinate> allPositions = new ArrayList<>();
		for (Iterator<Ship> i = ships.iterator(); i.hasNext();) {
			allPositions.addAll(i.next().getPosition());
		}
		return allPositions;
	}

	public static List<Coordinate> getAllShipHits(List<Ship> ships) {
		List<Coordinate> allHits = new ArrayList<>();
		for (Iterator<Ship> i = ships.iterator(); i.hasNext();) {
			allHits.addAll(i.next().getHits());
		}
		return allHits;
	}

	public static int getShipsAliveCount(List<Ship> ships) {
		int count = 0;
		for (Iterator<Ship> i = ships.iterator(); i.hasNext();) {
			if (!i.next().isDestroyed()) {
				count++;
			}
		}
		return count;
	}

	public static boolean allShipsDestroyed(List<Ship> ships) {
		return getShipsAliveCount(ships) == 0;
	}

}
