package me.danielhoyos.xl.commons.utils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import me.danielhoyos.xl.commons.model.Coordinate;
import me.danielhoyos.xl.commons.model.Game;
import me.danielhoyos.xl.commons.model.OpponentPlayer;
import me.danielhoyos.xl.commons.model.SelfPlayer;

/**
 * @author Daniel Hoyos
 *
 */
public class BoardUtils {
	
	private static final Logger logger = LoggerFactory.getLogger(BoardUtils.class);

	private final static char EMPTY_QUADRANT = '.';
	private final static char SHIP_QUADRANT = '*';
	private final static char MISSED_QUADRANT = '-';
	private final static char HIT_QUADRANT = 'X';

	public static void buildBoard(Game game) {
		buildSelfBoard (game.getSelf());
		buildOpponentBoard(game.getOpponent());
	}
	
	public static List<String> buildSelfBoard (SelfPlayer self) {
		//logger.info("Self Board"); //TODO Delete
		List<String> selfBoard = new ArrayList<>();
		char[][] board = initBoard();
		board = fillQuadrant(board, ShipUtils.getAllShipPositions(self.getShips()), SHIP_QUADRANT);
		board = fillQuadrant(board, self.getMissedShots(), MISSED_QUADRANT);
		board = fillQuadrant(board, ShipUtils.getAllShipHits(self.getShips()), HIT_QUADRANT);
		for (int i = 0; i < Constants.ROW_SIZE; i++) {
			selfBoard.add(new String(board[i]));
			//logger.info("  " + i + " ->\t" + new String(board[i]));//TODO Delete
		}
		return selfBoard;
	}
	
	public static List<String> buildOpponentBoard (OpponentPlayer opponent) {
		//logger.info("Opponent Board");//TODO Delete
		List<String> opponentBoard = new ArrayList<>();
		char[][] board = initBoard();
		board = fillQuadrant(board, opponent.getMissedShots(), MISSED_QUADRANT);
		board = fillQuadrant(board, opponent.getHitShots(), HIT_QUADRANT);
		for (int i = 0; i < Constants.ROW_SIZE; i++) {
			opponentBoard.add(new String(board[i]));
			//logger.info("  " + i + " ->\t" + new String(board[i]));//TODO Delete
		}
		return opponentBoard;
	}
	
	private static char[][] fillQuadrant (char[][] board, List<Coordinate> coordinates, char symbol){
		for (Iterator<Coordinate> i = coordinates.iterator(); i.hasNext(); ){
			Coordinate quadrant = i.next();
			board[quadrant.getRow()][quadrant.getColumn()] = symbol;
		}
		return board;
	}

	private static char[][] initBoard() {
		char[][] board = new char[Constants.ROW_SIZE][Constants.COLUMN_SIZE];
		for (int i = 0; i < Constants.ROW_SIZE; i++) {
			for (int j = 0; j < Constants.COLUMN_SIZE; j++) {
				board[i][j] = EMPTY_QUADRANT;
			}
		}
		return board;
	}

}
