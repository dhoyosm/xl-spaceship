package me.danielhoyos.xl.commons.utils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import me.danielhoyos.xl.commons.model.Coordinate;
import me.danielhoyos.xl.commons.model.OpponentPlayer;
import me.danielhoyos.xl.commons.model.Player;
import me.danielhoyos.xl.commons.model.Protocol;
import me.danielhoyos.xl.commons.model.SelfPlayer;
import me.danielhoyos.xl.commons.model.Ship;
import me.danielhoyos.xl.commons.utils.ShipUtils.Direction;
import me.danielhoyos.xl.commons.utils.ShipUtils.Type;

public class GameUtils {

	public static SelfPlayer createSelfPlayer(Player player) {
		SelfPlayer self = new SelfPlayer();
		self.setUserId(player.getUserId());
		self.setFullName(player.getFullName());
		self.setShips(createShips());
		return self;
	}

	public static OpponentPlayer createOpponentPlayer(String userId, String fullName) {
		OpponentPlayer opponent = new OpponentPlayer();
		opponent.setUserId(userId);
		opponent.setFullName(fullName);
		return opponent;
	}

	public static List<Ship> createShips() {
		Type[] shipTypes = Type.values();
		List<Coordinate> filledSpaces = new ArrayList<>();
		List<Ship> ships = new ArrayList<>();
		for (int i = 0; i < shipTypes.length; i++) {
			ships.add(createShip(shipTypes[i], filledSpaces));
		}
		return ships;
	}

	public static Ship createShip(Type type, List<Coordinate> filledSpaces) {
		List<Coordinate> initialPosition = new ArrayList<>();
		Direction[] directions = Direction.values();
		boolean validPosition = false;
		do {
			int iniRow = MathUtils.randomRow();
			int iniColumn = MathUtils.randomColumn();
			Direction direction = directions[MathUtils.randomGenerator(directions.length)];

			switch (type) {
			case A_CLASS:
				initialPosition = ShipUtils.getAClassInitialPosition(direction, iniRow, iniColumn);
				validPosition = isValidPosition(initialPosition, filledSpaces);
				if (validPosition) {
					filledSpaces.addAll(ShipUtils.getAClassHoleInitialPosition(direction, iniRow, iniColumn));
				}
				break;
			case B_CLASS:
				initialPosition = ShipUtils.getBClassInitialPosition(direction, iniRow, iniColumn);
				break;
			case S_CLASS:
				initialPosition = ShipUtils.getSClassInitialPosition(direction, iniRow, iniColumn);
				break;
			case ANGLE:
				initialPosition = ShipUtils.getAngleInitialPosition(direction, iniRow, iniColumn);
				break;
			case WINGER:
				initialPosition = ShipUtils.getWingerInitialPosition(direction, iniRow, iniColumn);
				break;
			}
			validPosition = validPosition ? validPosition : isValidPosition(initialPosition, filledSpaces);
		} while (!validPosition);

		filledSpaces.addAll(initialPosition);

		Ship ship = new Ship();
		ship.setShipName(type.toString());
		ship.setPosition(initialPosition);
		return ship;
	}

	public static boolean isValidPosition(List<Coordinate> initialPosition, List<Coordinate> filledSpaces) {
		if (initialPosition.isEmpty()) {
			return false;
		}
		for (Iterator<Coordinate> i = initialPosition.iterator(); i.hasNext();) {
			if (filledSpaces.contains(i.next())) {
				return false;
			}
		}
		return true;
	}
	
	public static Protocol getProtocolFromURL (String url) {
		String port = "";
		String host = "";
		String[] urlArr = url.split("http://|https://")[1].split(":");
		if(urlArr.length > 1) {
			host = urlArr[0];
			port = urlArr[1].split("/")[0];
		} else {
			host = urlArr[0].split("/")[0];
		}
		
		Protocol protocol = new Protocol();
		protocol.setHostname(host);
		protocol.setPort(port);
		
		return protocol;
	}

}
