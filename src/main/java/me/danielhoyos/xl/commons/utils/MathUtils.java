package me.danielhoyos.xl.commons.utils;

import java.util.Random;

/**
 * @author Daniel Hoyos
 *
 */
public class MathUtils {

	public static int randomGenerator(int min, int max) {
		Random random = new Random();
		return random.nextInt(max - min) + min;
	}

	public static int randomGenerator(int max) {
		return randomGenerator(0, max);
	}

	public static int randomRow() {
		return randomGenerator(0, Constants.ROW_SIZE);
	}

	public static int randomColumn() {
		return randomGenerator(0, Constants.COLUMN_SIZE);
	}

}
