/**
 * 
 */
package me.danielhoyos.xl.commons.model;

import java.util.List;

/**
 * @author Daniel Hoyos
 *
 */
public class SelfPlayer extends Player {

	private List<Ship> ships;

	public List<Ship> getShips() {
		return ships;
	}

	public void setShips(List<Ship> ships) {
		this.ships = ships;
	}

}
