package me.danielhoyos.xl.commons.model;

/**
 * @author Daniel Hoyos
 *
 */

public class Game {

	private String gameId;
	private SelfPlayer self;
	private OpponentPlayer opponent;
	private Protocol spaceshipProtol;
	private String turn;
	private String won;
	private boolean active;
	private boolean autopilotActive;
	private Rules rules;

	public String getGameId() {
		return gameId;
	}

	public void setGameId(String gameId) {
		this.gameId = gameId;
	}

	public SelfPlayer getSelf() {
		return self;
	}

	public void setSelf(SelfPlayer self) {
		this.self = self;
	}

	public OpponentPlayer getOpponent() {
		return opponent;
	}

	public void setOpponent(OpponentPlayer opponent) {
		this.opponent = opponent;
	}

	public Protocol getSpaceshipProtol() {
		return spaceshipProtol;
	}

	public void setSpaceshipProtol(Protocol spaceshipProtol) {
		this.spaceshipProtol = spaceshipProtol;
	}

	public String getTurn() {
		return turn;
	}

	public void setTurn(String turn) {
		this.turn = turn;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public String getWon() {
		return won;
	}

	public void setWon(String won) {
		this.won = won;
	}

	public boolean isAutopilotActive() {
		return autopilotActive;
	}

	public void setAutopilotActive(boolean autopilotActive) {
		this.autopilotActive = autopilotActive;
	}

	public Rules getRules() {
		return rules;
	}

	public void setRules(Rules rules) {
		this.rules = rules;
	}

}
