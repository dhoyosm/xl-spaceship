package me.danielhoyos.xl.commons.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

/**
 * @author Daniel Hoyos
 *
 */
@Entity
public class Player {
	@Id
	private String userId;
	private String fullName;
	@Transient
	private List<Coordinate> missedShots = new ArrayList<>();

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public List<Coordinate> getMissedShots() {
		return missedShots;
	}

	public void setMissedShots(List<Coordinate> missedShots) {
		this.missedShots = missedShots;
	}

}
