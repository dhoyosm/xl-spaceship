package me.danielhoyos.xl.commons.model;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Daniel Hoyos
 *
 */
public class OpponentPlayer extends Player {
	private List<Coordinate> hitShots = new ArrayList<>();

	public List<Coordinate> getHitShots() {
		return hitShots;
	}

	public void setHitShots(List<Coordinate> hitShots) {
		this.hitShots = hitShots;
	}

}
