package me.danielhoyos.xl.commons.model;

/**
 * @author Daniel Hoyos
 *
 */
public class Rules {

	private String rulesName;
	private int shots;

	public Rules() {
	}

	public Rules(String rulesName, int shots) {
		super();
		this.rulesName = rulesName;
		this.shots = shots;
	}

	public String getRulesName() {
		return rulesName;
	}

	public void setRulesName(String rulesName) {
		this.rulesName = rulesName;
	}

	public int getShots() {
		return shots;
	}

	public void setShots(int shots) {
		this.shots = shots;
	}

}
