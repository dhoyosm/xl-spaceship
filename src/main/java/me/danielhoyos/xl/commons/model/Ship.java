package me.danielhoyos.xl.commons.model;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Daniel Hoyos
 *
 */
public class Ship {

	private String shipName;
	private List<Coordinate> position = new ArrayList<>();
	private List<Coordinate> hits = new ArrayList<>();

	public String getShipName() {
		return shipName;
	}

	public void setShipName(String shipName) {
		this.shipName = shipName;
	}

	public List<Coordinate> getPosition() {
		return position;
	}

	public void setPosition(List<Coordinate> position) {
		this.position = position;
	}

	public List<Coordinate> getHits() {
		return hits;
	}

	public void setHits(List<Coordinate> hits) {
		this.hits = hits;
	}

	public boolean isDestroyed() {
		return this.position.size() == this.hits.size();
	}

}
