package me.danielhoyos.xl.commons.model;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author Daniel Hoyos
 *
 */
public class Protocol {

	@NotEmpty
	private String hostname;
	@NotEmpty
	private String port;

	public String getHostname() {
		return hostname;
	}

	public void setHostname(String hostname) {
		this.hostname = hostname;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}
}
