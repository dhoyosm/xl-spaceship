package me.danielhoyos.xl.commons.dto;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 * @author Daniel Hoyos
 *
 */
@Entity
public class GameDTO {

	@Id
	private String id;
	@Column(columnDefinition = "LONGVARCHAR")
	private String data;

	public GameDTO() {
		super();
	}

	public GameDTO(String id, String data) {
		super();
		this.id = id;
		this.data = data;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

}
