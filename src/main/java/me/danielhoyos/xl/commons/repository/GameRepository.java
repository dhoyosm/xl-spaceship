package me.danielhoyos.xl.commons.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import me.danielhoyos.xl.commons.dto.GameDTO;

/**
 * @author Daniel Hoyos
 *
 */
public interface GameRepository extends JpaRepository<GameDTO, String> {

}
