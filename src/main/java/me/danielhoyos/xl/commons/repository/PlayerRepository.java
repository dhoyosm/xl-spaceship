package me.danielhoyos.xl.commons.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import me.danielhoyos.xl.commons.model.Player;

/**
 * @author Daniel Hoyos
 *
 */
public interface PlayerRepository extends JpaRepository<Player, String> {

}
