package me.danielhoyos.xl.commons.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;

import me.danielhoyos.xl.commons.dto.GameDTO;
import me.danielhoyos.xl.commons.exception.XLException;
import me.danielhoyos.xl.commons.model.Game;
import me.danielhoyos.xl.commons.repository.GameRepository;
import me.danielhoyos.xl.commons.service.GameService;

/**
 * @author Daniel Hoyos
 *
 */
@Service
public class GameServiceImpl implements GameService {

	private Gson gson = new Gson();

	@Autowired
	GameRepository gameRepository;
	
	@Override
	public GameDTO saveGame(Game game) {
		return gameRepository.save(new GameDTO(game.getGameId(), gson.toJson(game)));
	}

	@Override
	public Game retreiveGame(String id) throws XLException {
		GameDTO gameDto = gameRepository.findOne(id);
		if (gameDto == null) {
			throw new XLException("Game '" + id + "' not found");
		}

		return gson.fromJson(gameDto.getData(), Game.class);
	}
	
	@Override
	public List<Game> retreiveGames() throws XLException{
		List<Game> games = new ArrayList<>();
		List<GameDTO> gamesDto =  gameRepository.findAll();
		for(Iterator<GameDTO> i = gamesDto.iterator(); i.hasNext(); ){
			games.add(gson.fromJson(i.next().getData(), Game.class));
		}
		return games;
	}
}
