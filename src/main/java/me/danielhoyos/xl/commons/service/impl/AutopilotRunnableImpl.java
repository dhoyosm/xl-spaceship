package me.danielhoyos.xl.commons.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import me.danielhoyos.xl.commons.exception.XLException;
import me.danielhoyos.xl.commons.model.Coordinate;
import me.danielhoyos.xl.commons.model.Game;
import me.danielhoyos.xl.commons.model.OpponentPlayer;
import me.danielhoyos.xl.commons.rest.model.ShotRequest;
import me.danielhoyos.xl.commons.service.GameService;
import me.danielhoyos.xl.commons.utils.MathUtils;
import me.danielhoyos.xl.user.service.UserShotService;

/**
 * @author Daniel Hoyos
 *
 */
@Service
public class AutopilotRunnableImpl implements Runnable {

	private static final Logger logger = LoggerFactory.getLogger(AutopilotRunnableImpl.class);

	@Autowired
	GameService gameService;

	@Autowired
	UserShotService shotService;

	@Override
	public void run() {
		try {
			Thread.sleep(3000);
			autopilotShot(Thread.currentThread().getName());
		} catch (InterruptedException | XLException e) {
			e.printStackTrace();
		}
	}

	private void autopilotShot(String gameId) throws XLException {
		logger.info("Autopilot shot for game: " + gameId);
		Game game = gameService.retreiveGame(gameId);
		ShotRequest salvo = new ShotRequest();
		salvo.setSalvo(createSalvo(game));
		shotService.fireShot(gameId, salvo);
	}

	private List<String> createSalvo(Game game) {
		List<String> salvo = new ArrayList<>();
		for (int i = 0; i < game.getRules().getShots(); i++) {
			Coordinate shot;
			int j = 0;
			boolean valid;
			do {
				valid = true;
				shot = new Coordinate(MathUtils.randomRow(), MathUtils.randomColumn());
				if (j < 10) {
					if (!isValidCoordinate(shot, game.getOpponent())) {
						valid = false;
					}
				}
				if (salvo.contains(salvo)) {
					valid = false;
				}
			} while (!valid);
			salvo.add(createSalvoShot(shot));
		}
		return salvo;
	}

	private boolean isValidCoordinate(Coordinate shot, OpponentPlayer opponent) {
		if (opponent.getMissedShots().contains(shot)) {
			return false;
		}
		if (opponent.getHitShots().contains(shot)) {
			return false;
		}
		return true;
	}

	private String createSalvoShot(Coordinate shot) {
		StringBuilder sb = new StringBuilder();
		sb.append(Integer.toHexString(shot.getRow()).toUpperCase());
		sb.append("x");
		sb.append(Integer.toHexString(shot.getColumn()).toUpperCase());
		return sb.toString();
	}

}
