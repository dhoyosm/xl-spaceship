package me.danielhoyos.xl.commons.service;

import java.util.List;

import me.danielhoyos.xl.commons.dto.GameDTO;
import me.danielhoyos.xl.commons.exception.XLException;
import me.danielhoyos.xl.commons.model.Game;

/**
 * @author Daniel Hoyos
 *
 */
public interface GameService {

	GameDTO saveGame(Game game);

	Game retreiveGame(String id) throws XLException;

	List<Game> retreiveGames() throws XLException;

}
