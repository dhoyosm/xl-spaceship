/**
 * 
 */
package me.danielhoyos.xl.commons.rest.model;

/**
 * @author Daniel Hoyos
 *
 */
public class GameStatusResponse {

	private PlayerResponse self = new PlayerResponse();
	private PlayerResponse opponent = new PlayerResponse();
	private GameResponse game = new GameResponse();

	public PlayerResponse getSelf() {
		return self;
	}

	public void setSelf(PlayerResponse self) {
		this.self = self;
	}

	public PlayerResponse getOpponent() {
		return opponent;
	}

	public void setOpponent(PlayerResponse opponent) {
		this.opponent = opponent;
	}

	public GameResponse getGame() {
		return game;
	}

	public void setGame(GameResponse game) {
		this.game = game;
	}

}
