package me.danielhoyos.xl.commons.rest.model;

import java.util.HashMap;

/**
 * @author Daniel Hoyos
 *
 */
public class ShotResponse {

	private HashMap<String, String> salvo = new HashMap<>();
	private GameResponse game;

	public HashMap<String, String> getSalvo() {
		return salvo;
	}

	public void setSalvo(HashMap<String, String> salvo) {
		this.salvo = salvo;
	}

	public GameResponse getGame() {
		return game;
	}

	public void setGame(GameResponse game) {
		this.game = game;
	}

}
