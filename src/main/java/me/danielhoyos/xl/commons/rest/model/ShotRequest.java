package me.danielhoyos.xl.commons.rest.model;

import java.util.List;

/**
 * @author Daniel Hoyos
 *
 */
public class ShotRequest {

	private List<String> salvo;

	public List<String> getSalvo() {
		return salvo;
	}

	public void setSalvo(List<String> salvo) {
		this.salvo = salvo;
	}

}
