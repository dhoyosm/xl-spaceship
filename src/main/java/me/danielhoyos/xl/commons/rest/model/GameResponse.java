package me.danielhoyos.xl.commons.rest.model;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * @author Daniel Hoyos
 *
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GameResponse {

	private String playerTurn;
	private String won;
	private boolean autopilot_active;
	private String rules;
	private int rulesShots;

	public boolean isAutopilot_active() {
		return autopilot_active;
	}

	public void setAutopilot_active(boolean autopilot_active) {
		this.autopilot_active = autopilot_active;
	}

	public String getPlayerTurn() {
		return playerTurn;
	}

	public void setPlayerTurn(String playerTurn) {
		this.playerTurn = playerTurn;
	}

	public String getWon() {
		return won;
	}

	public void setWon(String won) {
		this.won = won;
	}

	public String getRules() {
		return rules;
	}

	public void setRules(String rules) {
		this.rules = rules;
	}

	public int getRulesShots() {
		return rulesShots;
	}

	public void setRulesShots(int rulesShots) {
		this.rulesShots = rulesShots;
	}
}
