/**
 * 
 */
package me.danielhoyos.xl.commons.rest.model;

import org.hibernate.validator.constraints.NotEmpty;

import me.danielhoyos.xl.commons.model.Protocol;

/**
 * @author Daniel Hoyos
 *
 */
public class NewGameRequest {

	@NotEmpty
	String userId;
	@NotEmpty
	String fullName;
	Protocol spaceshipProtocol;
	String rules;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public Protocol getSpaceshipProtocol() {
		return spaceshipProtocol;
	}

	public void setSpaceshipProtocol(Protocol spaceshipProtocol) {
		this.spaceshipProtocol = spaceshipProtocol;
	}

	public String getRules() {
		return rules;
	}

	public void setRules(String rules) {
		this.rules = rules;
	}

}
