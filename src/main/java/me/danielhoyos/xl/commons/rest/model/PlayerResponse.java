/**
 * 
 */
package me.danielhoyos.xl.commons.rest.model;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Daniel Hoyos
 *
 */
public class PlayerResponse {
	private String userId;
	private List<String> board = new ArrayList<>();

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public List<String> getBoard() {
		return board;
	}

	public void setBoard(List<String> board) {
		this.board = board;
	}

}
