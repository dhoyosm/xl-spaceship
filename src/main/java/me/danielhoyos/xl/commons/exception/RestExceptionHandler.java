package me.danielhoyos.xl.commons.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * @author Daniel Hoyos
 *
 */
@ControllerAdvice
public class RestExceptionHandler {

	@ExceptionHandler(XLException.class)
	public ResponseEntity<ErrorResponse> exceptionXLHandler(XLException ex) {
		HttpStatus status = ex.getStatus() != null ? ex.getStatus() : HttpStatus.BAD_REQUEST;
		ErrorResponse error = new ErrorResponse();
		error.setErrorCode(status.value());
		error.setMessage(ex.getErrorMessage());
		return new ResponseEntity<ErrorResponse>(error, status);
	}

	@ExceptionHandler(Exception.class)
	public ResponseEntity<ErrorResponse> exceptionHandler(Exception ex) {
		ErrorResponse error = new ErrorResponse();
		error.setErrorCode(HttpStatus.BAD_REQUEST.value());
		error.setMessage(ex.getLocalizedMessage());
		// error.setMessage("The request could not be understood by the server
		// due to malformed syntax.");
		return new ResponseEntity<ErrorResponse>(error, HttpStatus.BAD_REQUEST);
	}
}
