package me.danielhoyos.xl.commons.exception;

import org.springframework.http.HttpStatus;

/**
 * @author Daniel Hoyos
 *
 */
public class XLException extends Exception {
	
	private static final long serialVersionUID = -4385893375403278978L;
	private String errorMessage;
	private HttpStatus status;

	public XLException() {
		super();
	}

	public XLException(String errorMessage) {
		super();
		this.errorMessage = errorMessage;
	}
	
	public XLException(String errorMessage, HttpStatus status) {
		super();
		this.errorMessage = errorMessage;
		this.status = status;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public HttpStatus getStatus() {
		return status;
	}

}
