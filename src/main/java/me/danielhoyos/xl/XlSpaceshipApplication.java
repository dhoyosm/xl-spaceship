package me.danielhoyos.xl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import me.danielhoyos.xl.commons.dto.GameDTO;
import me.danielhoyos.xl.commons.model.Game;
import me.danielhoyos.xl.commons.model.OpponentPlayer;
import me.danielhoyos.xl.commons.model.Player;
import me.danielhoyos.xl.commons.model.Protocol;
import me.danielhoyos.xl.commons.model.SelfPlayer;
import me.danielhoyos.xl.commons.model.Ship;
import me.danielhoyos.xl.commons.repository.GameRepository;
import me.danielhoyos.xl.commons.repository.PlayerRepository;
import me.danielhoyos.xl.commons.service.GameService;
import me.danielhoyos.xl.commons.utils.RulesUtils;

@SpringBootApplication
public class XlSpaceshipApplication {

	private static final Logger logger = LoggerFactory.getLogger(XlSpaceshipApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(XlSpaceshipApplication.class, args);
	}

	@Bean
	public CommandLineRunner setup(final GameRepository gameRepository, final PlayerRepository playerRepository,
			final GameService gameService) {

		return new CommandLineRunner() {

			@Value("${server.port}")
			int port;

			Gson gson = new Gson();

			String player1Ships = "[{ \"shipName\": \"A_CLASS\", \"position\": [{ \"row\": 10, \"column\": 5 }, { \"row\": 10, \"column\": 7 }, { \"row\": 11, \"column\": 5 }, { \"row\": 11, \"column\": 6 }, { \"row\": 11, \"column\": 7 }, { \"row\": 12, \"column\": 5 }, { \"row\": 12, \"column\": 7 }, { \"row\": 13, \"column\": 6 }], \"hits\": [] }, { \"shipName\": \"B_CLASS\", \"position\": [{ \"row\": 7, \"column\": 9 }, { \"row\": 7, \"column\": 10 }, { \"row\": 8, \"column\": 8 }, { \"row\": 8, \"column\": 10 }, { \"row\": 9, \"column\": 9 }, { \"row\": 9, \"column\": 10 }, { \"row\": 10, \"column\": 8 }, { \"row\": 10, \"column\": 10 }, { \"row\": 11, \"column\": 9 }, { \"row\": 11, \"column\": 10 }], \"hits\": [] }, { \"shipName\": \"S_CLASS\", \"position\": [{ \"row\": 5, \"column\": 14 }, { \"row\": 6, \"column\": 11 }, { \"row\": 6, \"column\": 13 }, { \"row\": 6, \"column\": 15 }, { \"row\": 7, \"column\": 11 }, { \"row\": 7, \"column\": 13 }, { \"row\": 7, \"column\": 15 }, { \"row\": 8, \"column\": 12 }], \"hits\": [] }, { \"shipName\": \"ANGLE\", \"position\": [{ \"row\": 10, \"column\": 0 }, { \"row\": 11, \"column\": 0 }, { \"row\": 12, \"column\": 0 }, { \"row\": 13, \"column\": 0 }, { \"row\": 13, \"column\": 1 }, { \"row\": 13, \"column\": 2 }], \"hits\": [] }, { \"shipName\": \"WINGER\", \"position\": [{ \"row\": 3, \"column\": 3 }, { \"row\": 3, \"column\": 4 }, { \"row\": 3, \"column\": 6 }, { \"row\": 3, \"column\": 7 }, { \"row\": 4, \"column\": 5 }, { \"row\": 5, \"column\": 3 }, { \"row\": 5, \"column\": 4 }, { \"row\": 5, \"column\": 6 }, { \"row\": 5, \"column\": 7 }], \"hits\": [] }]";
			String xebialabs1Ships = "[{ \"shipName\": \"A_CLASS\", \"position\": [{ \"row\": 11, \"column\": 3 }, { \"row\": 11, \"column\": 4 }, { \"row\": 11, \"column\": 5 }, { \"row\": 12, \"column\": 2 }, { \"row\": 12, \"column\": 4 }, { \"row\": 13, \"column\": 3 }, { \"row\": 13, \"column\": 4 }, { \"row\": 13, \"column\": 5 }], \"hits\": [] }, { \"shipName\": \"B_CLASS\", \"position\": [{ \"row\": 7, \"column\": 9 }, { \"row\": 7, \"column\": 10 }, { \"row\": 8, \"column\": 8 }, { \"row\": 8, \"column\": 10 }, { \"row\": 9, \"column\": 9 }, { \"row\": 9, \"column\": 10 }, { \"row\": 10, \"column\": 8 }, { \"row\": 10, \"column\": 10 }, { \"row\": 11, \"column\": 9 }, { \"row\": 11, \"column\": 10 }], \"hits\": [] }, { \"shipName\": \"S_CLASS\", \"position\": [{ \"row\": 2, \"column\": 3 }, { \"row\": 2, \"column\": 4 }, { \"row\": 3, \"column\": 2 }, { \"row\": 4, \"column\": 3 }, { \"row\": 4, \"column\": 4 }, { \"row\": 5, \"column\": 5 }, { \"row\": 6, \"column\": 3 }, { \"row\": 6, \"column\": 4 }], \"hits\": [] }, { \"shipName\": \"ANGLE\", \"position\": [{ \"row\": 0, \"column\": 12 }, { \"row\": 1, \"column\": 12 }, { \"row\": 2, \"column\": 12 }, { \"row\": 3, \"column\": 12 }, { \"row\": 3, \"column\": 13 }, { \"row\": 3, \"column\": 14 }], \"hits\": [] }, { \"shipName\": \"WINGER\", \"position\": [{ \"row\": 4, \"column\": 10 }, { \"row\": 4, \"column\": 11 }, { \"row\": 4, \"column\": 13 }, { \"row\": 4, \"column\": 14 }, { \"row\": 5, \"column\": 12 }, { \"row\": 6, \"column\": 10 }, { \"row\": 6, \"column\": 11 }, { \"row\": 6, \"column\": 13 }, { \"row\": 6, \"column\": 14 }], \"hits\": [] }]";

			@Override
			public void run(String... args) throws Exception {
				logger.info("Creating data for instance in Port: " + port);

				boolean isTestData = false;

				SelfPlayer self = new SelfPlayer();
				OpponentPlayer opponent = new OpponentPlayer();
				Protocol gameProtocol = new Protocol();

				switch (port) {
				case -1:
				case 8181:
					self.setUserId("player-1");
					self.setFullName("Assessment Player");
					self.setShips((List<Ship>) gson.fromJson(player1Ships, new TypeToken<List<Ship>>() {
					}.getType()));

					opponent.setUserId("xebialabs-1");
					opponent.setFullName("XebiaLabs Opponent");

					gameProtocol.setPort("9191");

					isTestData = true;
					break;
				case 9191:
					self.setUserId("xebialabs-1");
					self.setFullName("XebiaLabs Opponent");
					self.setShips((List<Ship>) gson.fromJson(xebialabs1Ships, new TypeToken<List<Ship>>() {
					}.getType()));

					opponent.setUserId("player-1");
					opponent.setFullName("Assessment Player");
					gameProtocol.setPort("8181");

					isTestData = true;
					break;
				default:
					String millis = String.valueOf(System.currentTimeMillis());
					long playerId = Long.valueOf(millis.substring(millis.length() - 7, millis.length()));
					self.setUserId("player-" + playerId);
					self.setFullName("Player " + playerId);
					isTestData = false;
				}

				if (isTestData) {
					gameProtocol.setHostname("127.0.0.1");

					Game game = new Game();
					game.setGameId("match-1");
					game.setSpaceshipProtol(gameProtocol);
					game.setTurn("player-1");
					game.setActive(true);
					game.setAutopilotActive(false);
					game.setRules(RulesUtils.DEFAULT_RULES);
					game.setSelf(self);
					game.setOpponent(opponent);
					gameRepository.save(new GameDTO("match-1", gson.toJson(game)));

					game.setGameId("match-2");
					game.setTurn("xebialabs-1");
					gameRepository.save(new GameDTO("match-2", gson.toJson(game)));

					game = gameService.retreiveGame("match-1");

					logger.info("The sample data has been generated:");
					logger.info("game_id: " + game.getGameId() + " | user_id: " + game.getSelf().getUserId()
							+ " | user_fullname: " + game.getSelf().getFullName());
				}

				Player player = new Player();
				player.setUserId(self.getUserId());
				player.setFullName(self.getFullName());
				playerRepository.save(player);

				logger.info("Player: " + gson.toJson(playerRepository.findAll()));

			}
		};

	}
}
