package me.danielhoyos.xl.user.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Daniel Hoyos
 *
 */
@RestController
public class TestController {

	@RequestMapping(value = "/test", method = RequestMethod.GET)
	public String getAlive() {
		return "I am alive!";
	}

}
