package me.danielhoyos.xl.user.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import me.danielhoyos.xl.commons.exception.XLException;
import me.danielhoyos.xl.commons.model.Game;
import me.danielhoyos.xl.commons.service.GameService;

/**
 * @author Daniel Hoyos
 *
 */
@RestController
@RequestMapping("/xl-spaceship/game")
public class GameController {

	@Autowired
	private GameService gameService;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ResponseEntity<List<Game>> gameStatus() throws XLException {

		List<Game> body = gameService.retreiveGames();

		return new ResponseEntity<List<Game>>(body, HttpStatus.OK);
	}

}
