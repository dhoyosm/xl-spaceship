package me.danielhoyos.xl.user.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import me.danielhoyos.xl.commons.exception.XLException;
import me.danielhoyos.xl.commons.rest.model.GameStatusResponse;
import me.danielhoyos.xl.commons.rest.model.NewGameRequest;
import me.danielhoyos.xl.commons.rest.model.NewGameResponse;
import me.danielhoyos.xl.commons.rest.model.ShotRequest;
import me.danielhoyos.xl.commons.rest.model.ShotResponse;
import me.danielhoyos.xl.user.service.UserGameService;
import me.danielhoyos.xl.user.service.UserShotService;

/**
 * @author Daniel Hoyos
 *
 */
@RestController
@RequestMapping("/xl-spaceship/user")
public class UserController {

	@Autowired
	private UserShotService shotService;

	@Autowired
	private UserGameService userGameService;

	@RequestMapping(value = "/game/{game_id}/fire", method = RequestMethod.POST)
	public ResponseEntity<ShotResponse> fireShot(@PathVariable("game_id") String gameId, @RequestBody ShotRequest salvo)
			throws XLException {

		ShotResponse body = shotService.fireShot(gameId, salvo);

		return new ResponseEntity<ShotResponse>(body, HttpStatus.OK);
	}

	@RequestMapping(value = "/game/{game_id}", method = RequestMethod.GET)
	public ResponseEntity<GameStatusResponse> getGameStatus(@PathVariable("game_id") String gameId) throws XLException {

		GameStatusResponse body = userGameService.getGameStatus(gameId);

		return new ResponseEntity<GameStatusResponse>(body, HttpStatus.OK);
	}

	@RequestMapping(value = "/game/{game_id}/auto", method = RequestMethod.POST)
	public ResponseEntity<String> startAutopilot(@PathVariable("game_id") String gameId) throws XLException {

		userGameService.startAutopilot(gameId);

		return new ResponseEntity<>("", HttpStatus.OK);
	}

	@RequestMapping(value = "/game/new", method = RequestMethod.POST)
	public ResponseEntity<NewGameResponse> requestNewGame(HttpServletRequest request, @RequestBody NewGameRequest input)
			throws XLException {

		String url = request.getRequestURL().toString();

		NewGameResponse body = userGameService.requestNewGame(input, url);

		return new ResponseEntity<NewGameResponse>(body, HttpStatus.SEE_OTHER);
	}

}
