package me.danielhoyos.xl.user.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import me.danielhoyos.xl.commons.exception.XLException;
import me.danielhoyos.xl.commons.model.Game;
import me.danielhoyos.xl.commons.rest.model.ShotRequest;
import me.danielhoyos.xl.commons.rest.model.ShotResponse;
import me.danielhoyos.xl.commons.service.GameService;
import me.danielhoyos.xl.commons.utils.ShotUtils;
import me.danielhoyos.xl.user.service.UserShotService;
import me.danielhoyos.xl.user.wsclient.ProtocolClient;

/**
 * @author Daniel Hoyos
 *
 */
@Service
public class UserShotServiceImpl implements UserShotService {

	@Autowired
	GameService gameService;

	@Autowired
	ProtocolClient client;

	@Override
	public ShotResponse fireShot(String gameId, ShotRequest salvo) throws XLException {
		if (gameId.isEmpty()) {
			throw new XLException("Missing 'game_id' parameter", HttpStatus.BAD_REQUEST);
		}

		Game game = gameService.retreiveGame(gameId);
		// validateTurn(game);
		ShotResponse response = client.fireShot(game, salvo);

		game.setOpponent(ShotUtils.processShotResponse(response.getSalvo(), game.getOpponent()));
		game.setTurn(response.getGame().getPlayerTurn());
		if (response.getGame().getWon() != null) {
			game.setWon(response.getGame().getWon());
			game.setActive(false);
		}

		gameService.saveGame(game);
		return response;
	}

}
