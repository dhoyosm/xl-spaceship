package me.danielhoyos.xl.user.service;

import me.danielhoyos.xl.commons.exception.XLException;
import me.danielhoyos.xl.commons.rest.model.GameStatusResponse;
import me.danielhoyos.xl.commons.rest.model.NewGameRequest;
import me.danielhoyos.xl.commons.rest.model.NewGameResponse;

/**
 * @author Daniel Hoyos
 *
 */
public interface UserGameService {

	NewGameResponse requestNewGame(NewGameRequest input, String url) throws XLException;

	GameStatusResponse getGameStatus(String gameId) throws XLException;

	void startAutopilot(String gameId) throws XLException;

}
