package me.danielhoyos.xl.user.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import me.danielhoyos.xl.commons.exception.XLException;
import me.danielhoyos.xl.commons.model.Game;
import me.danielhoyos.xl.commons.model.OpponentPlayer;
import me.danielhoyos.xl.commons.model.Player;
import me.danielhoyos.xl.commons.model.SelfPlayer;
import me.danielhoyos.xl.commons.repository.PlayerRepository;
import me.danielhoyos.xl.commons.rest.model.GameResponse;
import me.danielhoyos.xl.commons.rest.model.GameStatusResponse;
import me.danielhoyos.xl.commons.rest.model.NewGameRequest;
import me.danielhoyos.xl.commons.rest.model.NewGameResponse;
import me.danielhoyos.xl.commons.rest.model.PlayerResponse;
import me.danielhoyos.xl.commons.service.GameService;
import me.danielhoyos.xl.commons.service.impl.AutopilotRunnableImpl;
import me.danielhoyos.xl.commons.utils.BoardUtils;
import me.danielhoyos.xl.commons.utils.GameUtils;
import me.danielhoyos.xl.commons.utils.RulesUtils;
import me.danielhoyos.xl.user.service.UserGameService;
import me.danielhoyos.xl.user.wsclient.ProtocolClient;

/**
 * @author Daniel Hoyos
 *
 */
@Service
public class UserGameServiceImpl implements UserGameService {

	private static final Logger logger = LoggerFactory.getLogger(UserGameServiceImpl.class);

	@Autowired
	GameService gameService;

	@Autowired
	PlayerRepository playerRepository;

	@Autowired
	AutopilotRunnableImpl autopilot;

	@Autowired
	ProtocolClient client;

	@Override
	public NewGameResponse requestNewGame(NewGameRequest input, String url) throws XLException {

		Player player = playerRepository.findAll().get(0);

		NewGameRequest request = new NewGameRequest();
		request.setUserId(player.getUserId());
		request.setFullName(player.getFullName());
		request.setRules(input.getRules());
		request.setSpaceshipProtocol(GameUtils.getProtocolFromURL(url));

		NewGameResponse response = client.requestNewGame(request, input.getSpaceshipProtocol());

		SelfPlayer self = GameUtils.createSelfPlayer(player);
		OpponentPlayer opponent = GameUtils.createOpponentPlayer(response.getUserId(), response.getFullName());

		Game game = new Game();
		game.setGameId(response.getGameId());
		game.setOpponent(opponent);
		game.setSelf(self);
		game.setSpaceshipProtol(input.getSpaceshipProtocol());
		game.setActive(true);
		game.setTurn(response.getStarting());
		game.setRules(RulesUtils.newRules(response.getRules()));

		gameService.saveGame(game);

		return response;
	}

	@Override
	public GameStatusResponse getGameStatus(String gameId) throws XLException {
		Game game = gameService.retreiveGame(gameId);

		PlayerResponse self = new PlayerResponse();
		self.setUserId(game.getSelf().getUserId());
		self.setBoard(BoardUtils.buildSelfBoard(game.getSelf()));

		PlayerResponse opponent = new PlayerResponse();
		opponent.setUserId(game.getOpponent().getUserId());
		opponent.setBoard(BoardUtils.buildOpponentBoard(game.getOpponent()));

		GameResponse gameResponse = new GameResponse();
		if (!game.isActive()) {
			gameResponse.setWon(game.getWon());
		} else {
			gameResponse.setPlayerTurn(game.getTurn());
		}
		gameResponse.setAutopilot_active(game.isAutopilotActive());
		gameResponse.setRules(game.getRules().getRulesName());
		gameResponse.setRulesShots(game.getRules().getShots());

		GameStatusResponse response = new GameStatusResponse();
		response.setSelf(self);
		response.setOpponent(opponent);
		response.setGame(gameResponse);

		return response;
	}

	@Override
	public void startAutopilot(String gameId) throws XLException {
		Game game = gameService.retreiveGame(gameId);
		game.setAutopilotActive(true);
		gameService.saveGame(game);
		logger.info("Autopilot has been activated for game: " + gameId);
		Thread newThread = new Thread(autopilot, gameId);
		newThread.start();
	}
}
