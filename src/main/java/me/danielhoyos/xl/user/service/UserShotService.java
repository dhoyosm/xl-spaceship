package me.danielhoyos.xl.user.service;

import me.danielhoyos.xl.commons.exception.XLException;
import me.danielhoyos.xl.commons.rest.model.ShotRequest;
import me.danielhoyos.xl.commons.rest.model.ShotResponse;

/**
 * @author Daniel Hoyos
 *
 */
public interface UserShotService {

	ShotResponse fireShot(String gameId, ShotRequest salvo) throws XLException;

}
