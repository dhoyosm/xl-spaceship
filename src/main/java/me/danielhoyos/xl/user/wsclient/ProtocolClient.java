package me.danielhoyos.xl.user.wsclient;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import me.danielhoyos.xl.commons.exception.XLException;
import me.danielhoyos.xl.commons.model.Game;
import me.danielhoyos.xl.commons.model.Protocol;
import me.danielhoyos.xl.commons.rest.model.NewGameRequest;
import me.danielhoyos.xl.commons.rest.model.NewGameResponse;
import me.danielhoyos.xl.commons.rest.model.ShotRequest;
import me.danielhoyos.xl.commons.rest.model.ShotResponse;

/**
 * @author Daniel Hoyos
 *
 */
@Service
public class ProtocolClient {

	private final static String PROTOCOL_URL_BASE = "/xl-spaceship/protocol";

	private final RestTemplate restTemplate;

	public ProtocolClient(RestTemplateBuilder restTemplateBuilder) {
		this.restTemplate = restTemplateBuilder.build();
	}

	public ShotResponse fireShot(Game game, ShotRequest request) throws XLException {
		String path = PROTOCOL_URL_BASE + "/game/{game_id}";

		StringBuilder url = new StringBuilder("http://").append(game.getSpaceshipProtol().getHostname()).append(":")
				.append(game.getSpaceshipProtol().getPort()).append(path);
		try {
			return this.restTemplate.postForObject(url.toString(), request, ShotResponse.class, game.getGameId());
		} catch (Exception ex) {
			throw new XLException(ex.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}

	public NewGameResponse requestNewGame(NewGameRequest request, Protocol spaceshipProtocol) throws XLException {
		String path = PROTOCOL_URL_BASE + "/game/new";

		StringBuilder url = new StringBuilder("http://").append(spaceshipProtocol.getHostname()).append(":")
				.append(spaceshipProtocol.getPort()).append(path);
		try {
			return this.restTemplate.postForObject(url.toString(), request, NewGameResponse.class);
		} catch (Exception ex) {
			throw new XLException(ex.getMessage(), HttpStatus.BAD_REQUEST);
		}
	}

}
