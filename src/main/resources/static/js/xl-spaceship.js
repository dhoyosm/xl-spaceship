'use strict';

angular.module('xlSpaceshipApp', [])

.controller('XlSpaceshipController', function($scope, $http, $interval) {
    var game = this;
    game.gameStatus = {};

    $scope.salvo = [];
    $scope.newGameRulesSelect = ["standard", "super-charge", "desperation", "1-shot", "2-shot", "3-shot", "4-shot", "5-shot", "6-shot", "7-shot", "8-shot", "9-shot", "10-shot"];
    $scope.showNewGameForm = false;

    $scope.setNewGameRequest = function() {
        $scope.newGameRequest = {
            rules: "",
            spaceship_protocol: {
                hostname: "",
                port: ""
            }
        };
    };

    $scope.loadGames = function() {
        $http.get('/xl-spaceship/game/').then(
            function(response) {
                game.gameList = response.data;
                if (game.gameList) {
                    for (var i = 0; i < game.gameList.length; i++) {
                        var icon = "glyphicon-play";
                        var gameItem = game.gameList[i];
                        if (!gameItem.active) {
                            icon = "glyphicon-stop";
                        } else if (gameItem.autopilot_active) {
                            icon = "glyphicon-plane";
                        } else if (gameItem.turn === gameItem.opponent.user_id) {
                            icon = "glyphicon-pause";
                        }
                        game.gameList[i].icon = icon;
                    }
                }
            },
            function(error) {
                console.log(error.data);
            });
    };

    $scope.getGameStatus = function(gameId) {
        if (gameId) {
            $http.get('/xl-spaceship/user/game/' + gameId).then(
                function(response) {
                    game.gameStatus = response.data;
                    game.gameId = gameId;
                    for (var i = 0; i < game.gameStatus.self.board.length; i++) {
                        var boardRow = game.gameStatus.self.board[i].split('');
                        game.gameStatus.self.board[i] = boardRow;
                    }
                    for (var i = 0; i < game.gameStatus.opponent.board.length; i++) {
                        var boardRow = game.gameStatus.opponent.board[i].split('');
                        game.gameStatus.opponent.board[i] = boardRow;
                    }
                },
                function(error) {
                    console.log(error.data);
                });
        }
    };

    $scope.initBoard = function() {
        var newBoard = {
            self: {
                board: []
            },
            opponent: {
                board: []
            }
        };
        for (var i = 0; i < 16; i++) {
            var row = [];
            for (var j = 0; j < 16; j++) {
                row[j] = ".";
            }
            newBoard.self.board[i] = row;
            newBoard.opponent.board[i] = row;
        }
        game.gameStatus = newBoard;
    };

    $scope.loadGame = function(gameId) {
        $scope.getGameStatus(gameId);
        $scope.cleanSalvo();
        game.salvoResponse = {};
    };

    $scope.addShotToSalvo = function(val, row, col) {
        game.salvoResponse = {};
        var text = $scope.hexValue(row) + "x" + $scope.hexValue(col);
        var salvoIndex = $scope.salvo.indexOf(text);
        if (salvoIndex > -1) {
            $scope.salvo.splice(salvoIndex, 1);
            document.getElementById(text).dataset.selected = false;
        } else if ($scope.salvo.length < game.gameStatus.game.rules_shots) {
            $scope.salvo.push(text);
            document.getElementById(text).dataset.selected = true;
        }
    };

    $scope.shotsAvailable = function() {
        if (game.gameStatus.game) {
            return game.gameStatus.game.rules_shots - $scope.salvo.length;
        }
    };

    $scope.fireSalvo = function() {
        var requestData = {};
        requestData.salvo = $scope.salvo;
        $http.post('/xl-spaceship/user/game/' + game.gameId + '/fire/', JSON.stringify(requestData)).then(
            function(response) {
                $scope.cleanSalvo();
                game.salvoResponse = response.data;
            },
            function(error) {
                console.log(error.data);
            });
    };

    $scope.setAutopilot = function() {
        $http.post('/xl-spaceship/user/game/' + game.gameId + '/auto/')
            .then(function(response) {
                    game.gameStatus.game.autopilot_active = true;
                },
                function(error) {
                    console.log(error.data);
                });
    };

    $scope.requestNewGame = function() {
        $http.post('/xl-spaceship/user/game/new/', JSON.stringify($scope.newGameRequest))
            .then(function(response) {
                    $scope.setNewGameRequest();
                    $scope.showNewGameForm = false;
                },
                function(error) {
                    if (error.status === 303) {
                        $scope.setNewGameRequest();
                        $scope.showNewGameForm = false;
                    } else {
                        console.log(error.data);
                    }
                });
    };

    $scope.cancelRequestNewGame = function() {
        $scope.setNewGameRequest();
        $scope.showNewGameForm = false;
    };

    $scope.canFire = function() {
        if (game.gameStatus.game) {
            if ($scope.salvo.length !== game.gameStatus.game.rules_shots ||
                game.gameStatus.game.won ||
                (game.gameStatus.game.player_turn == game.gameStatus.opponent.user_id)) {
                return false;
            } else if (game.gameStatus.game.autopilot_active) {
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    };

    $scope.canActivateAutopilot = function() {
        if (game.gameStatus.game) {
            if (game.gameStatus.game.autopilot_active) {
                return false;
            } else if (!game.gameStatus.game ||
                (game.gameStatus.game.player_turn == game.gameStatus.opponent.user_id)) {
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }

    $scope.hexValue = function(val) {
        return val.toString(16).toUpperCase();
    };

    $scope.cleanSalvo = function() {
        for (var i = 0; i < $scope.salvo.length; i++) {
            document.getElementById($scope.salvo[i]).dataset.selected = false;
        }
        $scope.salvo = [];
    }

    $scope.quadrantColor = function(val) {
        if (val === "*") {
            return "#0066FF";
        }
        if (val === "X") {
            return "#A80000";
        }
        if (val === "-") {
            return "#66FFFF";
        }
        return "";
    };

    $scope.quadrantTooltip = function(val, row, col) {
        var text = $scope.hexValue(row) + "x" + $scope.hexValue(col);
        if (val === "*") {
            text = text + " It's a ship!!";
        }
        if (val === "X") {
            text = text + " Hit!";
        }
        if (val === "-") {
            text = text + " Missed!";
        }
        return text;
    }

    $scope.initBoard();
    $scope.loadGames();
    $scope.setNewGameRequest();

    $interval(function() {
        $scope.loadGames();
        $scope.getGameStatus(game.gameId);
    }, 2000);

    $(document).ready(function() {
        $('[data-toggle="tooltip"]').tooltip();
    });
})

;
