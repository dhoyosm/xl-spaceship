package me.danielhoyos.xl.protocol.rest.controller;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import me.danielhoyos.xl.XlSpaceshipApplication;

/**
 * @author Daniel Hoyos
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = XlSpaceshipApplication.class)
@SpringBootTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ProtocolControllerTest {
	private final static String URL = "/xl-spaceship/protocol";

	private MockMvc mockMvc;

	@Autowired
	private WebApplicationContext wac;
	
	@Before
	public void setup() {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
	}

	@Test
	public void verifyCreateGame() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.post(URL + "/game/new").contentType(MediaType.APPLICATION_JSON)
				.content("{"
						+ "\"user_id\": \"xebialabs-1\","
						+ "\"full_name\": \"XebiaLabs Opponent\","
						+ "\"spaceship_protocol\": {"
							+ "\"hostname\": \"127.0.0.1\","
							+ "\"port\": 9001"
							+ "}"
						+ "}")
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.user_id").exists())
				.andExpect(jsonPath("$.full_name").exists())
				.andExpect(jsonPath("$.game_id").exists())
				.andExpect(jsonPath("$.starting").exists())
				.andExpect(jsonPath("$.rules").exists())
				.andDo(print());
	}
	
	@Test
	public void verifyReceiveShot() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.post(URL + "/game/" + "match-1").contentType(MediaType.APPLICATION_JSON)
				.content("{\"salvo\": [\"0x0\", \"8x4\", \"DxA\", \"AxA\", \"7xF\"]}")
				.accept(MediaType.APPLICATION_JSON))
				.andDo(print());
	}

}
