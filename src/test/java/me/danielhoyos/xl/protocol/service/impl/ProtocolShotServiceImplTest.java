package me.danielhoyos.xl.protocol.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

import com.google.gson.Gson;

import me.danielhoyos.xl.XlSpaceshipApplication;
import me.danielhoyos.xl.commons.exception.XLException;
import me.danielhoyos.xl.commons.model.Game;
import me.danielhoyos.xl.commons.rest.model.ShotRequest;
import me.danielhoyos.xl.commons.rest.model.ShotResponse;
import me.danielhoyos.xl.commons.service.GameService;
import me.danielhoyos.xl.commons.service.impl.AutopilotRunnableImpl;
import me.danielhoyos.xl.commons.service.impl.XLTest;
import me.danielhoyos.xl.user.wsclient.ProtocolClient;

/**
 * @author Daniel Hoyos
 *
 */
@RunWith(PowerMockRunner.class)
@ContextConfiguration(classes = XlSpaceshipApplication.class)
@PrepareForTest(ProtocolShotServiceImpl.class)
@SpringBootTest
public class ProtocolShotServiceImplTest extends XLTest {
	
	private final static String SALVO = "{\"salvo\": [\"0x0\", \"8x4\", \"DxA\", \"AxA\", \"7xF\"]}";

	private String id = "match-2";
	private Gson gson = new Gson();
	private Game game = setTestGame(id, true);
	
	@Mock
	private GameService gameService;

	@Mock
	private ProtocolClient client;
	
	@Mock
	private AutopilotRunnableImpl autopilot;

	@InjectMocks
	private ProtocolShotServiceImpl shotService;

	@Before
	public void setup() {
		initMocks(this);
	}
	
	@Test
	public void testReceiveShot() throws XLException {
		ShotRequest salvo = gson.fromJson(SALVO, ShotRequest.class);
		game.setTurn(game.getOpponent().getUserId());

		when(gameService.retreiveGame(id)).thenReturn(game);

		ShotResponse result = shotService.receiveShot(id, salvo);

		verify(gameService).saveGame(any(Game.class));

		assertNotNull(result);
		assertEquals(5, result.getSalvo().size());
		assertEquals("player-1", result.getGame().getPlayerTurn());
	}
	@Test(expected = XLException.class)
	public void testReceiveShotEmptyGameId() throws XLException {
		shotService.receiveShot(new String(), null);
	}

	@Test(expected = XLException.class)
	public void testReceiveShotGameNotActive() throws XLException {
		game.setActive(false);

		when(gameService.retreiveGame(id)).thenReturn(game);

		shotService.receiveShot(id, null);
	}
	
	@Test(expected = XLException.class)
	public void testReceiveShotNotYourTurn() throws XLException {
		game.setTurn(game.getSelf().getUserId());

		when(gameService.retreiveGame(id)).thenReturn(game);

		shotService.receiveShot(id, null);
	}

	@Test(expected = XLException.class)
	public void testReceiveShotInvalidShot() throws XLException {
		String salvoJson = "{\"salvo\": [\"ZxK\"]}";
		ShotRequest salvo = gson.fromJson(salvoJson, ShotRequest.class);

		when(gameService.retreiveGame(id)).thenReturn(game);

		shotService.receiveShot(id, salvo);
	}

}