package me.danielhoyos.xl.protocol.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

import me.danielhoyos.xl.XlSpaceshipApplication;
import me.danielhoyos.xl.commons.exception.XLException;
import me.danielhoyos.xl.commons.model.Game;
import me.danielhoyos.xl.commons.model.Player;
import me.danielhoyos.xl.commons.model.Protocol;
import me.danielhoyos.xl.commons.repository.PlayerRepository;
import me.danielhoyos.xl.commons.rest.model.NewGameRequest;
import me.danielhoyos.xl.commons.rest.model.NewGameResponse;
import me.danielhoyos.xl.commons.service.GameService;
import me.danielhoyos.xl.commons.service.impl.XLTest;

/**
 * @author Daniel Hoyos
 *
 */
@RunWith(PowerMockRunner.class)
@ContextConfiguration(classes = XlSpaceshipApplication.class)
@PrepareForTest(ProtocolGameServiceImpl.class)
@SpringBootTest
public class ProtocolGameServiceImplTest extends XLTest {

	@Mock
	GameService gameService;

	@Mock
	PlayerRepository playerRepository;

	@InjectMocks
	private ProtocolGameServiceImpl protocolGameService;

	@Before
	public void setup() {
		initMocks(this);
		mockStatic(System.class);
	}

	@Test
	public void testCreateGame() throws XLException {
		List<Player> players = new ArrayList<>();
		players.add(setPlayer("player-1", "Player One"));

		NewGameRequest input = new NewGameRequest();
		input.setFullName("Player Two");
		input.setUserId("player-2");
		input.setRules("standard");
		input.setSpaceshipProtocol(new Protocol());

		when(playerRepository.findAll()).thenReturn(players);

		NewGameResponse result = protocolGameService.createGame(input);

		verify(gameService).saveGame(any(Game.class));

		assertNotNull(result);
		assertEquals("player-1", result.getUserId());
	}
}
