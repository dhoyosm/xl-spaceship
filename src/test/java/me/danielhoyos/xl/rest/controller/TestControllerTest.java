package me.danielhoyos.xl.rest.controller;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author Daniel Hoyos
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class TestControllerTest {

	@Autowired
	private TestRestTemplate restTemplate;

	@Test
	public void testAlive() throws Exception {
		String body = this.restTemplate.getForObject("/test", String.class);
		assertThat(body).isEqualTo("I am alive!");
	}

}
