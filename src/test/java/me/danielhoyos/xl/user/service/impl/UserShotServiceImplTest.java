package me.danielhoyos.xl.user.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import java.util.HashMap;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

import com.google.gson.Gson;

import me.danielhoyos.xl.XlSpaceshipApplication;
import me.danielhoyos.xl.commons.exception.XLException;
import me.danielhoyos.xl.commons.model.Game;
import me.danielhoyos.xl.commons.rest.model.GameResponse;
import me.danielhoyos.xl.commons.rest.model.ShotRequest;
import me.danielhoyos.xl.commons.rest.model.ShotResponse;
import me.danielhoyos.xl.commons.service.GameService;
import me.danielhoyos.xl.commons.service.impl.XLTest;
import me.danielhoyos.xl.user.wsclient.ProtocolClient;

/**
 * @author Daniel Hoyos
 *
 */
@RunWith(PowerMockRunner.class)
@ContextConfiguration(classes = XlSpaceshipApplication.class)
@PrepareForTest(UserShotServiceImpl.class)
@SpringBootTest
public class UserShotServiceImplTest extends XLTest {

	private final static String SALVO = "{\"salvo\": [\"0x0\", \"8x4\", \"DxA\", \"AxA\", \"7xF\"]}";

	private String id = "match-2";
	private Gson gson = new Gson();
	private Game game = setTestGame(id, true);

	@Mock
	private GameService gameService;

	@Mock
	private ProtocolClient client;

	@InjectMocks
	private UserShotServiceImpl shotService;

	@Before
	public void setup() {
		initMocks(this);
	}

	@Test
	public void testFireShot() throws XLException {
		ShotResponse response = mock(ShotResponse.class);
		GameResponse gameResponse = mock(GameResponse.class);
		HashMap<String, String> salvoResponse = new HashMap<>();
		salvoResponse.put("0x0", "miss");
		salvoResponse.put("AxA", "hit");

		ShotRequest salvo = gson.fromJson(SALVO, ShotRequest.class);

		when(gameService.retreiveGame(id)).thenReturn(game);
		when(client.fireShot(game, salvo)).thenReturn(response);
		when(response.getGame()).thenReturn(gameResponse);
		when(response.getSalvo()).thenReturn(salvoResponse);
		when(gameResponse.getPlayerTurn()).thenReturn("player-1");

		ShotResponse result = shotService.fireShot(id, salvo);

		verify(gameService).saveGame(any(Game.class));

		assertNotNull(result);
		assertEquals(2, result.getSalvo().size());
		assertEquals("player-1", result.getGame().getPlayerTurn());
	}

	@Test(expected = XLException.class)
	public void testFireShotEmptyGameId() throws XLException {
		shotService.fireShot(new String(), null);
	}

}
