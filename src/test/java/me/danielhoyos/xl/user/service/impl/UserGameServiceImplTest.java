package me.danielhoyos.xl.user.service.impl;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

import me.danielhoyos.xl.XlSpaceshipApplication;
import me.danielhoyos.xl.commons.exception.XLException;
import me.danielhoyos.xl.commons.model.Game;
import me.danielhoyos.xl.commons.model.Player;
import me.danielhoyos.xl.commons.model.Protocol;
import me.danielhoyos.xl.commons.repository.PlayerRepository;
import me.danielhoyos.xl.commons.rest.model.GameStatusResponse;
import me.danielhoyos.xl.commons.rest.model.NewGameRequest;
import me.danielhoyos.xl.commons.rest.model.NewGameResponse;
import me.danielhoyos.xl.commons.service.GameService;
import me.danielhoyos.xl.commons.service.impl.XLTest;
import me.danielhoyos.xl.user.wsclient.ProtocolClient;

/**
 * @author Daniel Hoyos
 *
 */
@RunWith(PowerMockRunner.class)
@ContextConfiguration(classes = XlSpaceshipApplication.class)
@PrepareForTest(UserGameServiceImpl.class)
@SpringBootTest
public class UserGameServiceImplTest extends XLTest {

	@Mock
	private GameService gameService;

	@Mock
	private PlayerRepository playerRepository;

	@Mock
	private ProtocolClient client;

	@InjectMocks
	private UserGameServiceImpl userGameService;

	@Before
	public void setup() {
		initMocks(this);
		mockStatic(System.class);
	}

	@Test
	public void testRequestNewGame() throws XLException {
		String url = "http://localhost:8080";
		Protocol protocol = new Protocol();
		protocol.setHostname("localhost");
		protocol.setPort("9090");
		NewGameRequest input = new NewGameRequest();
		input.setRules("standard");
		input.setSpaceshipProtocol(protocol);

		NewGameResponse response = mock(NewGameResponse.class);

		List<Player> players = new ArrayList<>();
		players.add(setPlayer("player-1", "Player One"));

		when(playerRepository.findAll()).thenReturn(players);
		when(client.requestNewGame(any(NewGameRequest.class), eq(input.getSpaceshipProtocol()))).thenReturn(response);

		NewGameResponse result = userGameService.requestNewGame(input, url);

		verify(gameService).saveGame(any(Game.class));

		assertNotNull(result);
	}

	@Test
	public void testGetGameStatus() throws XLException {
		String gameId = "Test-1";

		when(gameService.retreiveGame(gameId)).thenReturn(setTestGame(gameId, true));

		GameStatusResponse result = userGameService.getGameStatus(gameId);

		assertNotNull(result);
		assertNotNull(result.getSelf());
		assertNotNull(result.getOpponent());
		assertNotNull(result.getGame());
		assertNull(result.getGame().getWon());
		assertNotNull(result.getGame().getPlayerTurn());
	}

	@Test
	public void testGetGameStatusInactive() throws XLException {
		String gameId = "Test-1";

		when(gameService.retreiveGame(gameId)).thenReturn(setTestGame(gameId, false));

		GameStatusResponse result = userGameService.getGameStatus(gameId);

		assertNotNull(result);
		assertNotNull(result.getSelf());
		assertNotNull(result.getOpponent());
		assertNotNull(result.getGame());
		assertNotNull(result.getGame().getWon());
		assertNull(result.getGame().getPlayerTurn());
	}

	@Test
	public void testStartAutopilot() throws XLException {
		String gameId = "Test-1";

		when(gameService.retreiveGame(gameId)).thenReturn(setTestGame(gameId, false));

		userGameService.startAutopilot(gameId);

		verify(gameService).saveGame(any(Game.class));
	}

}
