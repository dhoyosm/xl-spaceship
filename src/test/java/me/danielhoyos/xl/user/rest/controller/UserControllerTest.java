package me.danielhoyos.xl.user.rest.controller;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import me.danielhoyos.xl.XlSpaceshipApplication;

/**
 * @author Daniel Hoyos
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = XlSpaceshipApplication.class)
@TestPropertySource(properties = { "port: -1" })
@SpringBootTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class UserControllerTest {
	private final static String URL = "/xl-spaceship/user";

	private MockMvc mockMvc;

	@Autowired
	private WebApplicationContext wac;

	@Before
	public void setup() {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
	}

	@Test
	public void verifyFireShot() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.post(URL + "/game/match-1/fire").contentType(MediaType.APPLICATION_JSON)
				.content("{\"salvo\": [\"0x0\", \"8x4\", \"DxA\", \"AxA\", \"7xF\"]}")
				.accept(MediaType.APPLICATION_JSON)).andDo(print());
	}

	@Test
	public void verifyGetGameStatus() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.get(URL + "/game/match-1").contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON)).andExpect(jsonPath("$.self").exists())
				.andExpect(jsonPath("$.self.user_id").exists()).andExpect(jsonPath("$.self.board").exists())
				.andExpect(jsonPath("$.opponent").exists()).andExpect(jsonPath("$.opponent.user_id").exists())
				.andExpect(jsonPath("$.opponent.board").exists()).andExpect(jsonPath("$.game").exists()).andDo(print());
	}
	
	@Test
	public void verifyStartAutopilot() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.post(URL + "/game/match-1/auto").contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON)).andDo(print());
	}
	
	@Test
	public void verifyRequestNewGame() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.post(URL + "/game/new").contentType(MediaType.APPLICATION_JSON)
				.content("{ \"rules\": \"standard\", \"spaceship_protocol\": { \"hostname\": \"localhost\", \"port\": 8080 } }")
				.accept(MediaType.APPLICATION_JSON))
				.andDo(print());
	}

}
