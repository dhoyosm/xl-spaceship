package me.danielhoyos.xl.commons.utils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import me.danielhoyos.xl.commons.model.Coordinate;
import me.danielhoyos.xl.commons.utils.Constants;
import me.danielhoyos.xl.commons.utils.ShipUtils;
import me.danielhoyos.xl.commons.utils.ShipUtils.Direction;

/**
 * @author Daniel Hoyos
 *
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest(ShipUtils.class)
public class ShipUtilsTest {

	@Test
	public void testGetAClassInitialPositionDEG_0() {
		List<Coordinate> response = ShipUtils.getAClassInitialPosition(Direction.DEG_0, 1, 1);
		assertNotNull(response);
		assertEquals(8, response.size());
		assertTrue(response.contains(new Coordinate(1, 2)));
		assertTrue(response.contains(new Coordinate(4, 3)));
	}

	@Test
	public void testGetAClassInitialPositionDEG_90() {
		List<Coordinate> response = ShipUtils.getAClassInitialPosition(Direction.DEG_90, 1, 1);
		assertNotNull(response);
		assertEquals(8, response.size());
		assertTrue(response.contains(new Coordinate(1, 1)));
		assertTrue(response.contains(new Coordinate(3, 3)));
	}

	@Test
	public void testGetAClassInitialPositionDEG_180() {
		List<Coordinate> response = ShipUtils.getAClassInitialPosition(Direction.DEG_180, 1, 1);
		assertNotNull(response);
		assertEquals(8, response.size());
		assertTrue(response.contains(new Coordinate(1, 1)));
		assertTrue(response.contains(new Coordinate(2, 3)));
	}

	@Test
	public void testGetAClassInitialPositionDEG_270() {
		List<Coordinate> response = ShipUtils.getAClassInitialPosition(Direction.DEG_270, 1, 1);
		assertNotNull(response);
		assertEquals(8, response.size());
		assertTrue(response.contains(new Coordinate(1, 2)));
		assertTrue(response.contains(new Coordinate(3, 4)));
	}

	@Test
	public void testGetAClassHoleInitialPositionDEG_0() {
		List<Coordinate> response = ShipUtils.getAClassHoleInitialPosition(Direction.DEG_0, 1, 1);
		assertNotNull(response);
		assertEquals(1, response.size());
		assertTrue(response.contains(new Coordinate(2, 2)));
	}

	@Test
	public void testGetAClassHoleInitialPositionDEG_90() {
		List<Coordinate> response = ShipUtils.getAClassHoleInitialPosition(Direction.DEG_90, 1, 1);
		assertNotNull(response);
		assertEquals(1, response.size());
		assertTrue(response.contains(new Coordinate(2, 3)));
	}

	@Test
	public void testGetAClassHoleInitialPositionDEG_180() {
		List<Coordinate> response = ShipUtils.getAClassHoleInitialPosition(Direction.DEG_180, 1, 1);
		assertNotNull(response);
		assertEquals(1, response.size());
		assertTrue(response.contains(new Coordinate(3, 2)));
	}

	@Test
	public void testGetAClassHoleInitialPositionDEG_270() {
		List<Coordinate> response = ShipUtils.getAClassHoleInitialPosition(Direction.DEG_270, 1, 1);
		assertNotNull(response);
		assertEquals(1, response.size());
		assertTrue(response.contains(new Coordinate(2, 2)));
	}

	@Test
	public void testGetBClassInitialPositionDEG_0() {
		List<Coordinate> response = ShipUtils.getBClassInitialPosition(Direction.DEG_0, 1, 1);
		assertNotNull(response);
		assertEquals(10, response.size());
		assertTrue(response.contains(new Coordinate(1, 1)));
		assertTrue(response.contains(new Coordinate(5, 2)));
	}

	@Test
	public void testGetBClassInitialPositionDEG_90() {
		List<Coordinate> response = ShipUtils.getBClassInitialPosition(Direction.DEG_90, 1, 1);
		assertNotNull(response);
		assertEquals(10, response.size());
		assertTrue(response.contains(new Coordinate(1, 1)));
		assertTrue(response.contains(new Coordinate(3, 4)));
	}

	@Test
	public void testGetBClassInitialPositionDEG_180() {
		List<Coordinate> response = ShipUtils.getBClassInitialPosition(Direction.DEG_180, 1, 1);
		assertNotNull(response);
		assertEquals(10, response.size());
		assertTrue(response.contains(new Coordinate(1, 2)));
		assertTrue(response.contains(new Coordinate(5, 3)));
	}

	@Test
	public void testGetBClassInitialPositionDEG_270() {
		List<Coordinate> response = ShipUtils.getBClassInitialPosition(Direction.DEG_270, 1, 1);
		assertNotNull(response);
		assertEquals(10, response.size());
		assertTrue(response.contains(new Coordinate(1, 2)));
		assertTrue(response.contains(new Coordinate(3, 5)));
	}

	@Test
	public void testGetSClassInitialPositionDEG_0() {
		List<Coordinate> response = ShipUtils.getSClassInitialPosition(Direction.DEG_0, 1, 1);
		assertNotNull(response);
		assertEquals(8, response.size());
		assertTrue(response.contains(new Coordinate(1, 2)));
		assertTrue(response.contains(new Coordinate(5, 3)));
	}

	@Test
	public void testGetSClassInitialPositionDEG_90() {
		List<Coordinate> response = ShipUtils.getSClassInitialPosition(Direction.DEG_90, 1, 1);
		assertNotNull(response);
		assertEquals(8, response.size());
		assertTrue(response.contains(new Coordinate(1, 4)));
		assertTrue(response.contains(new Coordinate(4, 2)));
	}

	@Test
	public void testGetSClassInitialPositionDEG_180() {
		List<Coordinate> response = ShipUtils.getSClassInitialPosition(Direction.DEG_180, 1, 1);
		assertNotNull(response);
		assertEquals(8, response.size());
		assertTrue(response.contains(new Coordinate(1, 2)));
		assertTrue(response.contains(new Coordinate(5, 3)));
	}

	@Test
	public void testGetSClassInitialPositionDEG_270() {
		List<Coordinate> response = ShipUtils.getSClassInitialPosition(Direction.DEG_270, 1, 1);
		assertNotNull(response);
		assertEquals(8, response.size());
		assertTrue(response.contains(new Coordinate(1, 4)));
		assertTrue(response.contains(new Coordinate(4, 2)));
	}

	@Test
	public void testGetAngleInitialPositionDEG_0() {
		List<Coordinate> response = ShipUtils.getAngleInitialPosition(Direction.DEG_0, 1, 1);
		assertNotNull(response);
		assertEquals(6, response.size());
		assertTrue(response.contains(new Coordinate(1, 1)));
		assertTrue(response.contains(new Coordinate(4, 3)));
	}

	@Test
	public void testGetAngleInitialPositionDEG_90() {
		List<Coordinate> response = ShipUtils.getAngleInitialPosition(Direction.DEG_90, 1, 1);
		assertNotNull(response);
		assertEquals(6, response.size());
		assertTrue(response.contains(new Coordinate(1, 1)));
		assertTrue(response.contains(new Coordinate(3, 1)));
	}

	@Test
	public void testGetAngleInitialPositionDEG_180() {
		List<Coordinate> response = ShipUtils.getAngleInitialPosition(Direction.DEG_180, 1, 1);
		assertNotNull(response);
		assertEquals(6, response.size());
		assertTrue(response.contains(new Coordinate(1, 1)));
		assertTrue(response.contains(new Coordinate(4, 3)));
	}

	@Test
	public void testGetAngleInitialPositionDEG_270() {
		List<Coordinate> response = ShipUtils.getAngleInitialPosition(Direction.DEG_270, 1, 1);
		assertNotNull(response);
		assertEquals(6, response.size());
		assertTrue(response.contains(new Coordinate(1, 4)));
		assertTrue(response.contains(new Coordinate(3, 4)));
	}

	@Test
	public void testGetWingerInitialPositionDEG_0() {
		List<Coordinate> response = ShipUtils.getWingerInitialPosition(Direction.DEG_0, 1, 1);
		assertNotNull(response);
		assertEquals(9, response.size());
		assertTrue(response.contains(new Coordinate(1, 1)));
		assertTrue(response.contains(new Coordinate(5, 3)));
	}

	@Test
	public void testGetWingerInitialPositionDEG_90() {
		List<Coordinate> response = ShipUtils.getWingerInitialPosition(Direction.DEG_90, 1, 1);
		assertNotNull(response);
		assertEquals(9, response.size());
		assertTrue(response.contains(new Coordinate(1, 1)));
		assertTrue(response.contains(new Coordinate(3, 5)));
	}

	@Test
	public void testGetWingerInitialPositionDEG_180() {
		List<Coordinate> response = ShipUtils.getWingerInitialPosition(Direction.DEG_180, 1, 1);
		assertNotNull(response);
		assertEquals(9, response.size());
		assertTrue(response.contains(new Coordinate(1, 1)));
		assertTrue(response.contains(new Coordinate(5, 3)));
	}

	@Test
	public void testGetWingerInitialPositionDEG_270() {
		List<Coordinate> response = ShipUtils.getWingerInitialPosition(Direction.DEG_270, 1, 1);
		assertNotNull(response);
		assertEquals(9, response.size());
		assertTrue(response.contains(new Coordinate(1, 1)));
		assertTrue(response.contains(new Coordinate(3, 5)));
	}

	@Test
	public void testPositionHigherThanRow() {
		List<Coordinate> response = ShipUtils.getWingerInitialPosition(Direction.DEG_270, Constants.ROW_SIZE, 1);
		assertNotNull(response);
		assertTrue(response.isEmpty());
	}

	@Test
	public void testPositionHigherThanColumn() {
		List<Coordinate> response = ShipUtils.getWingerInitialPosition(Direction.DEG_270, 1, Constants.COLUMN_SIZE);
		assertNotNull(response);
		assertTrue(response.isEmpty());
	}
}
