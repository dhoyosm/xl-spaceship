package me.danielhoyos.xl.commons.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;

import me.danielhoyos.xl.commons.dto.GameDTO;
import me.danielhoyos.xl.commons.model.Coordinate;
import me.danielhoyos.xl.commons.model.Game;
import me.danielhoyos.xl.commons.model.OpponentPlayer;
import me.danielhoyos.xl.commons.model.Player;
import me.danielhoyos.xl.commons.model.Rules;
import me.danielhoyos.xl.commons.model.SelfPlayer;
import me.danielhoyos.xl.commons.model.Ship;
import me.danielhoyos.xl.commons.utils.RulesUtils;

/**
 * @author Daniel Hoyos
 *
 */
public class XLTest {
	protected static final String GAME_PREFIX = "match-";

	private Gson gson = new Gson();

	protected Game setTestGame(String gameId, boolean active) {
		List<Coordinate> missedShots = new ArrayList<>();
		List<Coordinate> hitShots = new ArrayList<>();
		List<Coordinate> shipCoordinates = new ArrayList<>();
		List<Ship> ships = new ArrayList<>();

		missedShots.add(new Coordinate(0, 0));
		hitShots.add(new Coordinate(3, 3));
		shipCoordinates.add(new Coordinate(3, 3));
		shipCoordinates.add(new Coordinate(5, 5));

		Ship ship = new Ship();
		ship.setPosition(shipCoordinates);
		ship.setHits(hitShots);
		ships.add(ship);

		SelfPlayer self = new SelfPlayer();
		self.setUserId("player-1");
		self.setShips(ships);
		self.setMissedShots(missedShots);

		OpponentPlayer opponent = new OpponentPlayer();
		opponent.setUserId("opponent-1");
		opponent.setHitShots(hitShots);
		opponent.setMissedShots(missedShots);
		
		Rules rules = RulesUtils.DEFAULT_RULES;

		Game game = new Game();
		game.setGameId(gameId);
		game.setSelf(self);
		game.setOpponent(opponent);
		game.setActive(active);
		game.setAutopilotActive(false);
		game.setTurn(self.getUserId());
		game.setWon(opponent.getUserId());
		game.setRules(rules);

		return game;
	}

	protected GameDTO setTestGameDTO(String gameId, boolean active) {
		Game game = setTestGame(gameId, active);
		return new GameDTO(gameId, gson.toJson(game));
	}
	
	protected Player setPlayer(String userId, String fullName) {
		Player player = new Player();
		player.setUserId(userId);
		player.setFullName(fullName);
		return player;
	}
}
