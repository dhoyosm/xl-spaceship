package me.danielhoyos.xl.commons.service.impl;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

import me.danielhoyos.xl.XlSpaceshipApplication;
import me.danielhoyos.xl.commons.exception.XLException;
import me.danielhoyos.xl.commons.rest.model.ShotRequest;
import me.danielhoyos.xl.commons.service.GameService;
import me.danielhoyos.xl.commons.service.impl.AutopilotRunnableImpl;
import me.danielhoyos.xl.user.service.UserShotService;

/**
 * @author Daniel Hoyos
 *
 */
@RunWith(PowerMockRunner.class)
@ContextConfiguration(classes = XlSpaceshipApplication.class)
@PrepareForTest(AutopilotRunnableImpl.class)
@SpringBootTest
public class AutopilotRunnableImplTest extends XLTest {

	@Mock
	private UserShotService shotService;

	@Mock
	private GameService gameService;

	@InjectMocks
	private AutopilotRunnableImpl autopilotRunnableImpl;

	@Test
	public void testRun() throws XLException {

		when(gameService.retreiveGame(anyString())).thenReturn(setTestGame("Test-1", false));

		autopilotRunnableImpl.run();

		verify(shotService).fireShot(anyString(), any(ShotRequest.class));
	}

}
