package me.danielhoyos.xl.commons.service.impl;

import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

import me.danielhoyos.xl.XlSpaceshipApplication;
import me.danielhoyos.xl.commons.dto.GameDTO;
import me.danielhoyos.xl.commons.exception.XLException;
import me.danielhoyos.xl.commons.model.Game;
import me.danielhoyos.xl.commons.repository.GameRepository;

/**
 * @author Daniel Hoyos
 *
 */
@RunWith(PowerMockRunner.class)
@ContextConfiguration(classes = XlSpaceshipApplication.class)
@PrepareForTest(GameServiceImpl.class)
@SpringBootTest
public class GameServiceImplTest extends XLTest {

	@Mock
	private GameRepository gameRepository;

	@InjectMocks
	private GameServiceImpl gameService;

	@Before
	public void setup() {
		initMocks(this);
		mockStatic(System.class);
	}

	@Test
	public void testSaveGame() {
		String id = "Test-0";
		GameDTO gameDto = new GameDTO();
		gameDto.setId(id);
		Game game = new Game();
		game.setGameId(id);

		when(gameRepository.save((any(GameDTO.class)))).thenReturn(gameDto);

		GameDTO result = gameService.saveGame(game);

		verify(gameRepository).save(any(GameDTO.class));

		assertNotNull(result);
		assertEquals(id, result.getId());
	}

	@Test
	public void testRetreiveGame() throws XLException {
		String id = "Test-1";
		GameDTO gameDto = new GameDTO();
		gameDto.setId(id);
		gameDto.setData("{\"gameId\":\"Test-1\"}");

		when(gameRepository.findOne(id)).thenReturn(gameDto);

		Game result = gameService.retreiveGame("Test-1");

		verify(gameRepository).findOne(id);
		assertNotNull(result);
		assertEquals("Test-1", result.getGameId());
	}

	@Test(expected = XLException.class)
	public void testRetreiveGameNotFound() throws XLException {
		String id = "Test-1";
		when(gameRepository.findOne(id)).thenReturn(null);

		gameService.retreiveGame(id);
	}

}
