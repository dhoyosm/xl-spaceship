# README #

Welcome to XL Spaceship assessment test.

## What is on this folder? ##

* *xl-spaceship-[x.x].jar*: executable jar of the project. Version x.x
* *pom.xml*: Maven pom file with the information about about the project and dependencies.
* *src* folder: contains all the source code for the solution.
* *README.md* (this): Instructions on how to build and execute the project.

## How to run the project? ##

* To run the project, double-click on the executable jar file. It would star one instance of the project one the defaul port (8080).
* The project can also be started by executing it from the command line:
```
	java -jar xl-spaceship-<version>.jar [--port=<port_number>]
```

## About the ports ##

* The project can be executed on any ports available by adding the *--port* argument to the execution. There are some consideratios:
* Default Port: 8080
* Test Ports:
	In order to create pre-populated test data, there are two local instances that can be created, one on port 8181 and the other on 9191.
	* Port 8181: Creates the instance of "player-1" against "xebialabs-1" who is on port "9191" for the game "match-1"
	* Port 9191: Creates the instance of "xebialabs-1" against "player-1" who is on port "8181" for the game "match-1"

## How to build the code? ##

* Prerequisites:
	* Java SDK 1.7+
	* Maven 3.3 +

* In the command console go to the the root folder where the pom.xml is located
* Execute the following command:
```
	mvn clean install package
```
* This command will generate an executable file on target/ folder

